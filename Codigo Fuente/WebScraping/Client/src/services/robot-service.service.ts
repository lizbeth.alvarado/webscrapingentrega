import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../../settings';

@Injectable({
  providedIn: 'root'
})
export class RobotServiceService {

  constructor(private http: HttpClient) { }

  GetComercios(): any[] {
    let data;
    this.http.get(AppSettings.API_ENDPOINT + 'comercio').subscribe((res) => {
      data = res;
      console.log(data);
    });
    return data;
  }

  GetSources(IdComercio, Criterio): any[] {
    let data;
    this.http.get(AppSettings.API_ENDPOINT + 'sources/' + IdComercio + '/' + Criterio).subscribe((res) => {
      data = res;
    });;
    return data;
  }
}
