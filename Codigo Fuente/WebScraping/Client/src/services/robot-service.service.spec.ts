import { TestBed } from '@angular/core/testing';

import { RobotServiceService } from './robot-service.service';

describe('RobotServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RobotServiceService = TestBed.get(RobotServiceService);
    expect(service).toBeTruthy();
  });
});
