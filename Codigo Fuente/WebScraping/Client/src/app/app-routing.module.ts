import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScrapComponent } from './scrap/scrap.component';
import { ProductosComponent } from './productos/productos.component';

const routes: Routes = [
  { path: 'Scrap', component: ScrapComponent },
  { path: 'Productos', component: ProductosComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
