import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../../../settings';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  public loading = false;
  productos: any = [];
  productosPaginacion: any = [];
  displayedColumns: string[] = ['nombre', 'descripcion', 'precio', 'comercio'];
  longitud: number;
  indicePagina: number;
  opcionesPaginado: any = [];
  pageEvent: PageEvent;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.opcionesPaginado = [5, 10, 25, 100];
    this.loading = true;
    this.http.get(AppSettings.API_ENDPOINT + 'producto').subscribe((res) => {
      this.loading = false;
      this.productos = res;
      this.longitud = this.productos.length;
      this.indicePagina = 0;
      this.productosPaginacion = this.productos.slice(0, 10);
    });
  }

  Pagina(event: PageEvent): void {
    console.log(event);
    var initIndex = event.pageIndex * event.pageSize;
    this.productosPaginacion = this.productos.slice(initIndex, initIndex + event.pageSize);
  }

}
