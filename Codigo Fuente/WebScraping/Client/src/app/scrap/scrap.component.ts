import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RobotServiceService } from '../../../src/services/robot-service.service';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../../../settings';

@Component({
  selector: 'app-scrap',
  templateUrl: './scrap.component.html',
  styleUrls: ['./scrap.component.css'],
  providers: [RobotServiceService]
})
export class ScrapComponent implements OnInit {
  
  public loading = false;
  comercios: any;
  productos: any = [];
  displayedColumns: string[] = ['nombre', 'descripcion', 'precio', 'comercio'];
  scrapForm = this.fb.group({
    IdComercio: ['', Validators.required],
    Criterio: ['', Validators.required]
  });  

  constructor(private fb: FormBuilder,
    private http: HttpClient, 
    private robotService: RobotServiceService) { }

  ngOnInit() {
    this.loading = true;
    this.http.get(AppSettings.API_ENDPOINT + 'comercio').subscribe((res) => {
      this.loading = false;
      this.comercios = res;
    });
    
  }

  Busca(): void {
     console.log(this.scrapForm.value);
     this.loading = true;
     this.http.get(AppSettings.API_ENDPOINT + 'sources/' + this.scrapForm.value.IdComercio + '/' + this.scrapForm.value.Criterio)
     .subscribe((res) => {
       this.loading = false;
      this.productos = res;
      console.log(this.productos);
    });
  }

}
