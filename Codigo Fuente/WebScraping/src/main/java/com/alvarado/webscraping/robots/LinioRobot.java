/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.robots;

import com.alvarado.webscraping.model.AccessDB;
import com.alvarado.webscraping.model.Comercio;
import com.alvarado.webscraping.utils.Config;
import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author ThinkPad
 */
public class LinioRobot implements IRobot {
    private static Comercio LINIO;
    private final AccessDB accessdb;
    private Properties props;
    
    private static final String URL = "https://www.linio.com.mx/";
    private static final String INPUT_SEARCH_XPATH = "//*[@id=\"navbar\"]/div[1]/div[2]/div[4]/form/div/div[2]/input";
    private static final String BUTTON_SEARCH_XPATH = "//*[@id=\"navbar\"]/div[1]/div[2]/div[4]/form/div/div[3]/button";
    private static final String RESULTS_TITLE_XPATH = "//*[@id=\"catalogue-list-container\"]/h1";
    private static final String BASE_PRODUCT_DESCRIPTION_XPATH = "//*[@id=\"catalogue-product-container\"]/div[";
    private static final String POS_PRODUCT_DESCRIPTION_XPATH = "]/a/div[2]/p/span";
    private static final String POS_PRODUCT_PRICE_XPATH = "]/a/div[2]/div/div/div/span";    

    public LinioRobot() throws IOException {
        this.accessdb = new AccessDB("PU");
        LINIO = accessdb.getComercioByNombre("Linio");
        props = Config.getXMLInternalProperties("config.xml");
    }

    
    @Override
    public void Scrap(String keywords) {
        try {
            System.setProperty("webdriver.chrome.driver", props.getProperty("chromedriver"));

            ChromeDriver driver = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(driver, 30);
            driver.navigate().to(URL);
            driver.findElement(By.xpath(INPUT_SEARCH_XPATH)).sendKeys(keywords);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BUTTON_SEARCH_XPATH))).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RESULTS_TITLE_XPATH)));

            for (int i = 1; i < 11; i++) {
                try {
                    WebElement descriptionWE = driver.findElement(By.xpath(BASE_PRODUCT_DESCRIPTION_XPATH + i + POS_PRODUCT_DESCRIPTION_XPATH));
                    String description = descriptionWE.getText();

                    WebElement priceWE = driver.findElement(By.xpath(BASE_PRODUCT_DESCRIPTION_XPATH + i + POS_PRODUCT_PRICE_XPATH));
                    accessdb.GuardaProducto(description, priceWE.getText(), description.split(",")[0], LINIO);
                } catch (Exception e) {
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
