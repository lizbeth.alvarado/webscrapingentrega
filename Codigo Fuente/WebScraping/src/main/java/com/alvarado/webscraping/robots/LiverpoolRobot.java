/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.robots;

import com.alvarado.webscraping.model.AccessDB;
import com.alvarado.webscraping.model.Comercio;
import com.alvarado.webscraping.utils.Config;
import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author ThinkPad
 */
public class LiverpoolRobot implements IRobot {
    private static Comercio LIVERPOOL;
    private final AccessDB accessdb;
    private Properties props;
    
    private static final String URL = "https://www.liverpool.com.mx/tienda/home";
    private static final String INPUT_SEARCH_XPATH = "//*[@id=\"__next\"]/header/div[5]/div[2]/div/div/div/div[3]/div/div/input";
    private static final String BUTTON_SEARCH_XPATH = "//*[@id=\"__next\"]/header/div[5]/div[2]/div/div/div/div[3]/div/div/div/button";
    private static final String RESULTS_TITLE_XPATH = "//*[@id=\"__next\"]/div[1]/div/div[3]/main/div[1]/div/div/div[2]/p";
    private static final String BASE_PRODUCT_DESCRIPTION_XPATH = "//*[@id=\"__next\"]/div[1]/div/div[3]/main/div[2]/div/div/ul/li[";
    private static final String POS_PRODUCT_DESCRIPTION_XPATH = "]/div/figure/figcaption/article/h5";
    private static final String POS_PRODUCT_PRICE_XPATH = "]/div/figure/figcaption/p[2]";
    private static final String PRE_PRODUCT_PRICE_FRAGMENT_XPATH = "//*[@id=\"__next\"]/div[1]/div/div[3]/main/div[2]/div/div/ul/li[";
    private static final String POS_PRODUCT_PRICE_FRAGMENT_XPATH = "]/div/figure/figcaption/p[2]/sup";

    public LiverpoolRobot () throws IOException {
        accessdb = new AccessDB("PU");
        LIVERPOOL = accessdb.getComercioByNombre("Liverpool");
        props = Config.getXMLInternalProperties("config.xml");
    }

    @Override
    public void Scrap(String keywords) {
        try {
            System.setProperty("webdriver.chrome.driver", props.getProperty("chromedriver"));

            ChromeDriver driver = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(driver, 30);
            driver.navigate().to(URL);
            driver.findElement(By.xpath(INPUT_SEARCH_XPATH)).sendKeys(keywords);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BUTTON_SEARCH_XPATH))).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RESULTS_TITLE_XPATH)));

            for (int i = 1; i < 11; i++) {
                try {
                    WebElement descriptionWE = driver.findElement(By.xpath(BASE_PRODUCT_DESCRIPTION_XPATH + i + POS_PRODUCT_DESCRIPTION_XPATH));
                    String description = descriptionWE.getText();

                    WebElement priceWE = driver.findElement(By.xpath(BASE_PRODUCT_DESCRIPTION_XPATH + i + POS_PRODUCT_PRICE_XPATH));
                    WebElement fractionpriceWE = driver.findElement(By.xpath(PRE_PRODUCT_PRICE_FRAGMENT_XPATH + i + POS_PRODUCT_PRICE_FRAGMENT_XPATH));
                    
                    String price = priceWE.getText();
                    accessdb.GuardaProducto(description, price.substring(0, price.length()-2) + "." + fractionpriceWE.getText(), description.split(",")[0], LIVERPOOL);
                } catch (Exception e) {
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
