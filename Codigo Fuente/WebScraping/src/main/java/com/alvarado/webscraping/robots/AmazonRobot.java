/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.robots;

import com.alvarado.webscraping.model.AccessDB;
import com.alvarado.webscraping.model.Comercio;
import com.alvarado.webscraping.model.Producto;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author ThinkPad
 */
public class AmazonRobot implements IRobot {

    private static final String inputSearchXPath = "//*[@id=\"twotabsearchtextbox\"]";
    private static final String searchButtonXpath = "//*[@id=\"nav-search\"]/form/div[2]/div/input";
    private static final String searchResultsListDivXpath = "//*[@id=\"search\"]/div[1]/div[2]/div/span[3]/div[1]";
    private static final String baseProductsXpath = "//*[@id=\"search\"]/div[1]/div[2]/div/span[3]/div[1]/div[";
    private static final String posDescriptionsXpath = "]/div/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a/span";
    private static final String posDescriptionsXpathAlt = "]/div/div/div/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a/span";
    private static final String posPricesXpath = "]/div/div/div/div[2]/div[2]/div/div[2]/div[1]/div/div[1]/div/div/a/span/span[2]/span[2]";
    private static final String posPricesXpathAlt = "]/div/div/div/div/div/div[2]/div[2]/div/div[2]/div[1]/div/div[1]/div/div/a/span/span[2]/span[2]";
    private static final String fractionPriceXpath = "]/div/div/div/div[2]/div[2]/div/div[2]/div[1]/div/div[1]/div/div/a/span/span[2]/span[3]";
    private static final String fractionPriceXpathAlt = "]/div/div/div/div/div/div[2]/div[2]/div/div[2]/div[1]/div/div[1]/div/div/a/span/span[2]/span[3]";
    private static Comercio AMAZON;
    private final AccessDB accessdb;

    public AmazonRobot() throws IOException {
        accessdb = new AccessDB("PU");
        AMAZON = accessdb.getComercioByNombre("Amazon");
    }    

    @Override
    public void Scrap(String keywords) {
        try {
            System.setProperty("webdriver.chrome.driver", "C:\\DRIVERS\\chrome\\chromedriver.exe");

            ChromeDriver driver = new ChromeDriver();
            WebDriverWait wait = new WebDriverWait(driver, 30);
            driver.navigate().to("https://www.amazon.com.mx/");
            driver.findElement(By.xpath(inputSearchXPath)).sendKeys(keywords);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(searchButtonXpath))).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(searchResultsListDivXpath)));

            for (int i = 1; i < 11; i++) {
                try {
                    WebElement descriptionWE = driver.findElement(By.xpath(baseProductsXpath + i + posDescriptionsXpath));
                    String description = descriptionWE.getText();

                    WebElement priceWE = driver.findElement(By.xpath(baseProductsXpath + i + posPricesXpath));
                    WebElement fractionpriceWE = driver.findElement(By.xpath(baseProductsXpath + i + fractionPriceXpath));
                    accessdb.GuardaProducto(description, priceWE.getText() + "." + fractionpriceWE.getText(), description.split(",")[0], AMAZON);
                } catch (Exception e) {
                    WebElement descriptionWE = driver.findElement(By.xpath(baseProductsXpath + i + posDescriptionsXpathAlt));
                    String description = descriptionWE.getText();

                    WebElement priceWE = driver.findElement(By.xpath(baseProductsXpath + i + posPricesXpathAlt));
                    WebElement fractionpriceWE = driver.findElement(By.xpath(baseProductsXpath + i + fractionPriceXpathAlt));
                    accessdb.GuardaProducto(description, priceWE.getText() + "." + fractionpriceWE.getText(), description.split(",")[0], AMAZON);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
