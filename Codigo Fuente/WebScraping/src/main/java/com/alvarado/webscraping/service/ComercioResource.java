/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.service;

import com.alvarado.webscraping.model.AccessDB;
import com.alvarado.webscraping.model.Comercio;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author ThinkPad
 */
@Path("/comercio")
public class ComercioResource {

    @Context
    private UriInfo context;

    private AccessDB acceso = new AccessDB("PU");
    /**
     * Creates a new instance of ComercioResource
     */
    public ComercioResource() {
    }

    /**
     * Retrieves representation of an instance of com.alvarado.webscraping.service.ComercioResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Comercio> getComercios() {
        return acceso.getComercios();
    }
    
}
