/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.service;

import com.alvarado.webscraping.model.AccessDB;
import com.alvarado.webscraping.model.Comercio;
import com.alvarado.webscraping.model.Producto;
import com.alvarado.webscraping.robots.AmazonRobot;
import com.alvarado.webscraping.robots.IRobot;
import com.alvarado.webscraping.robots.LinioRobot;
import com.alvarado.webscraping.robots.LiverpoolRobot;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ThinkPad
 */
@Path("/sources")
public class SourcesResource {

    private AccessDB acceso = new AccessDB("PU");

    @GET
    @Path("{IdComercio}/{Criterio}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Producto> GetFromSources(
            @PathParam("IdComercio") int IdComercio, 
            @PathParam("Criterio") String Criterio) {
        Comercio comercio = acceso.getComercioById(IdComercio);
        List<Producto> productos = null;
        try {
            Date fechaConsulta = Calendar.getInstance(Locale.US).getTime();
            IRobot robot = null;
            switch (comercio.getNombre()) {
                case "Amazon":
                    robot = new AmazonRobot();
                    break;
                case "Liverpool":
                    robot = new LiverpoolRobot();
                    break;
                case "Linio":
                    robot = new LinioRobot();
                    break;
            }
            robot.Scrap(Criterio);
            productos = acceso.GetByDate(fechaConsulta);
        } catch (Exception e) {
        }
        return productos;
    }
}
