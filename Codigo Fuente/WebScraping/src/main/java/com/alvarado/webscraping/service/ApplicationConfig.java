/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author ThinkPad
 */
public class ApplicationConfig extends Application {
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }
    private void addRestResourceClasses(Set<Class<?>> resources) {
        //add all resources classes
        resources.add(com.alvarado.webscraping.service.ComercioResource.class);
        resources.add(com.alvarado.webscraping.service.ProductoResource.class);        
        resources.add(com.alvarado.webscraping.service.SourcesResource.class);

    }
}
