/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ThinkPad
 */
public class EMFactory {
    private final EntityManagerFactory entityManagerFactory;

    public EMFactory(String persistanceUnit) {
        entityManagerFactory =
                Persistence.createEntityManagerFactory(persistanceUnit);
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public EntityManager getEntitymanager() {
        return entityManagerFactory.createEntityManager();
    }
    
}
