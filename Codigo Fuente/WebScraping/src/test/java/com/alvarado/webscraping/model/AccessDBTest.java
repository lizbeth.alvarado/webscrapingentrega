/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alvarado.webscraping.model;

import java.math.BigDecimal;
import java.util.List;
import org.hamcrest.core.IsNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ThinkPad
 */
public class AccessDBTest {
    
    public AccessDBTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of GetAllProductos method, of class AccessDB.
     */
    @Test
    public void testGetAllProductos() {
        System.out.println("GetAllProductos");
        AccessDB instance = new AccessDB("PU");
        List<Producto> result = instance.GetAllProductos();
        assertThat(result, IsNull.notNullValue());
    }
    
}
