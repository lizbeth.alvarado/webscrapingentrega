Instrucciones de instalación

Prerrequisitos:
1. Apache Tomcat 9
2. Postgresql 11.5

BASE DE DATOS
1. Ejecute el script webscraping_schema.sql
2. Ejecute el script webscraping_data.sql

CONFIGURACIÓN DEL DRIVER 

1. Descargue el archivo chromedriver.exe en la sección descargas de la siguiente url: https://chromedriver.chromium.org/
2. Desempaquete el contenido del archivo en la siguiente ubicación: C:\DRIVERS\chrome

APLICACIÓN WEB
1. Ingrese a la url: http://localhost:8080/
2. Click en Manager App 
3. Proporcione usuario y contraseña (debe tener roles manager-gui, manager-script), en caso de no tener este dato, abra un editor de texto en modo administrador, 
   y abra el archivo tomcat-users.xml agregue la siguiente línea
   <user username="admin" password="admin" roles="manager-gui, manager-script" />
   debajo de<tomcat-users xmlns="http://tomcat.apache.org/xml"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd"
              version="1.0">
4. Las credenciales recién agregadas son: 
	usuario: admin
	contraseña: admin
5. Dentro de manager app, ir a la sección Archivo WAR a desplegar, seleccione el archivo WebScraping.war y de click en Desplegar
6. Espere un momento a que cargue. Ir al apartado de Aplicaciones, buscar /WebScraping, y click en él

PROBANDO LA APLICACIÓN
La aplicación accede a base datos con el usuario postgres, contraseña admin, en caso de que deba cambiar este dato, realizar lo siguiente:

1. Detener el servicio de Apache Tomcat
2. Ir a la ubicación: C:\Program Files\Apache Software Foundation\Tomcat 9.0\webapps\WebScraping\WEB-INF\classes\META-INF
3. Abrir el archivo persistence.xml con privilegios de administrador
4. Modificar los datos de usuario y contraseña
5. Guardar los cambios
6. Iniciar apache tomcat

